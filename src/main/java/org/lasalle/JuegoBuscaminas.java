package org.lasalle;

import java.util.Scanner;

public class JuegoBuscaminas {
  

  public static void jugar(boolean mostrarSolucion){
    System.out.println("****Buscaminas****");
    Buscaminas buscaminas = new Buscaminas();
    buscaminas.generarMinas(3);
    if (mostrarSolucion)buscaminas.mostrarSolucion();
    buscaminas.actualizar();

    int x, y;

    while(true){
      if(buscaminas.esFinalizado() == true && buscaminas.terminadoConVictoria() == true){
        System.out.println("Has ganado!");
        buscaminas.mostrarSolucion();
        break;
      }else if(buscaminas.esFinalizado() == true){
        buscaminas.mostrarSolucion();
        break;
      }else if(buscaminas.esFinalizado() == false){
        x = -1;
        y = -1;     //Resets values.
        y = leerCoordenada("Entra la coordenada x correspondiente a la columna:");
        x = leerCoordenada("Entra la coordenada y correspondiente a la fila:");
        buscaminas.seleccionaCasilla(x,y);
        buscaminas.hayGanador();
        buscaminas.detectarMinas();
        buscaminas.actualizar();
      }
      
    }   
  }

  public static int leerCoordenada(String frase){
    Scanner scan = new Scanner(System.in);
    boolean valorCorrecto = false;
    int coordenada=0;
    do {
      System.out.println(frase);
      valorCorrecto = scan.hasNextInt();

      if(valorCorrecto == false){
        scan.nextLine();
        System.out.println("Escriba un valor entero!");
      }else{
        coordenada = scan.nextInt();
        scan.nextLine();

        if(coordenada < 1 && coordenada > 3){
          System.out.println("Escriba una coordenada entre 1 y 3");
          valorCorrecto = false;
        }else{
          valorCorrecto = true;
        }
      }
    }while(valorCorrecto == false);
    return coordenada;
  }
}
